<%--
  Created by IntelliJ IDEA.
  User: Parisa Nikzad
  Date: 7/12/2017
  Time: 11:07 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Date Interval Web Application</title>
  </head>
  <body>
  <h1>Date Range Calculation </h1>
  <p> Please Enter the Date in following format: MMMM d, yyyy <br /> Example: April 24, 2015 </p>

  <form action="BetaSevlet" method="post">
    <table style="border: 1px solid black; ">
      <tr><td>Old Date</td> <td><input type="text" name="oldDate"/> </td></tr>
      <tr><td>New Date</td> <td><input type="text" name="newDate"/></td> </tr>
      <tr> <td><input type="submit" value="submit"/> </td></tr>
    </table>
  </form>

  </body>
</html>
