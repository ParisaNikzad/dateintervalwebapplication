import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.Date;

/**
 * Created by Parisa Nikzad on 7/12/2017.
 */
@WebServlet(name = "BetaSevlet")
public class BetaSevlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String oldDate = request.getParameter("oldDate");
        String newDate = request.getParameter("newDate");

        try{
            Date d1 = new SimpleDateFormat("MMMM d, yyyy").parse(oldDate);
            Instant instant1 = d1.toInstant();
            ZonedDateTime zdt1 = instant1.atZone(ZoneId.systemDefault());
            java.time.LocalDate date1 = zdt1.toLocalDate();

            Date d2 = new SimpleDateFormat("MMMM d, yyyy").parse(newDate);
            Instant instant2 = d2.toInstant();
            ZonedDateTime zdt2 = instant2.atZone(ZoneId.systemDefault());
            java.time.LocalDate date2 = zdt2.toLocalDate();

            java.time.Period p = java.time.Period.between(date1, date2);

            String result = String.format("You are " + p.getYears() + " years, " + p.getMonths() +
                    " months, and " + p.getDays() +
                    " days");
            PrintWriter out = response.getWriter();
            out.println(result);

        }catch(Exception e){
            PrintWriter out = response.getWriter();
            out.println("There is an error");
        }


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
